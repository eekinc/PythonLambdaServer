from contextlib import contextmanager
from importlib import util
import json
import os
import sys
import traceback

# https://stackoverflow.com/questions/41861427/python-3-5-how-to-dynamically-import-a-module-given-the-full-file-path-in-the
@contextmanager
def add_to_path(p):
    import sys
    old_path = sys.path
    sys.path = sys.path[:]
    sys.path.insert(0, p)
    try:
            yield
    finally:
            sys.path = old_path

def path_import(absolute_path):
    '''
    implementation taken from https://docs.python.org/3/library/importlib.html#importing-a-source-file-directly
    '''

    with add_to_path(os.path.dirname(absolute_path)):
        spec = util.spec_from_file_location(absolute_path, absolute_path)
        module = util.module_from_spec(spec)
        spec.loader.exec_module(module)
        return module

def main(lambdaFilePath, eventString):
    event = json.loads(eventString)
    lambdaFile = path_import(lambdaFilePath)

    try:
        result = lambdaFile.lambda_handler(event, None)
    except Exception as e:
        result = {
            'errorMessage': str(type(e)) + ': ' + str(e),
            'trace': traceback.format_exc().split('\n')
        }
        f = open('log.txt', 'w')
        traceback.print_exc(file=f)
        f.close()
        f = open('log.txt', 'r')
        lines = f.readlines()
        f.close()
        result = {'errorMessage': lines}
    print(json.dumps(result))

if __name__ == '__main__':
        main(*sys.argv[1:])