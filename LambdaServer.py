#!/usr/bin/python3

from argparse import ArgumentParser
import cgi
import glob
from http.server import BaseHTTPRequestHandler, HTTPServer
from os.path import join, sep
import json
from socketserver import ThreadingTCPServer
import subprocess
from urllib.parse import parse_qs, urlparse

class LambdaServer:
    def __init__(self, directory):
        self._dir = directory
        self._lambdas = self.listLambdas()

    def listLambdas(self):
        '''
        Recursively find all lambdas under a directory.
        Looks for files like: **/my_lambda_name/lambda_function.py
        '''
        result = dict()

        path = join(self._dir, '**', 'lambda_function.py')
        paths = glob.glob(path, recursive=True)
        for path in paths:
            lambdaName = path.split(sep)[-2]
            result[lambdaName] = path

        return result

    def callLambda(self, lambdaName, eventData):
        '''
        Calls a lambda function by name
        '''
        lambdaF = self._lambdas[lambdaName]
        # command = 'python3 LambdaExecutor.py ' + lambdaF + ' "' + eventData + '"'
        result = subprocess.run(['python3', 'LambdaExecutor.py', lambdaF, eventData], stdout=subprocess.PIPE)
        if result.stderr is not None:
            raise RuntimeError(result.stderr)

        return result.stdout.decode()

    class RequestHandler(BaseHTTPRequestHandler):
        def sendJson(self, s):
            self.send_response(200)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(s.encode())

        def do_POST(self):
            ctype, pdict = cgi.parse_header(self.headers['content-type'])
            if ctype == 'multipart/form-data':
                pdict['boundary'] = pdict['boundary'].encode()
                postvars = cgi.parse_multipart(self.rfile, pdict)
            elif ctype == 'application/x-www-form-urlencoded':
                length = int(self.headers['content-length'])
                postvars = parse_qs(self.rfile.read(length), keep_blank_values=1)
            else:
                postvars = {}

            lambdaName = postvars['FunctionName'.encode()][0].decode()
            payload = postvars['Payload'.encode()][0].decode()

            result = LambdaServerApp.server.callLambda(lambdaName, payload)

            self.sendJson(result)

    def run(self, port):
        server_address = ('', port)
        httpd = ThreadingTCPServer(server_address, LambdaServer.RequestHandler)
        httpd.serve_forever()

class LambdaServerApp:
    server = None

    @staticmethod
    def parseArgs():
        parser = ArgumentParser()
        parser.add_argument('--directory', type=str,
            help='Parent directory containing your Python lambda code.')
        parser.add_argument('--port', type=int,
            help='Port to listen to for incoming lambda calls.')

        return parser.parse_args()

if __name__ == '__main__':
    args = LambdaServerApp.parseArgs()
    LambdaServerApp.server = LambdaServer(args.directory)
    LambdaServerApp.server.run(args.port)