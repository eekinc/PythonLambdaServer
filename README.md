# Python Lambda Server
Host your Python 3.6 AWS lambdas locally for development and testing purposes.

## Usage
1. Point your lambda client to http://localhost:&lt;port&gt; in development (see "Frontend Usage")
2. `$ ./LambdaServer --directory=path/to/my/lambda/code --port=5001`
3. Run your app.

## Frontend Usage
Eg:
```
AWS.Lambda(...).invoke = (params, callback) => {
    return Request.post('http://localhost:5001', params, {}, true /* Crossorigin */)
        .then(result => {
            result = {'Payload': result};
            callback(null, result);
        }, err => {
            callback(err, null);
        });
    }
```